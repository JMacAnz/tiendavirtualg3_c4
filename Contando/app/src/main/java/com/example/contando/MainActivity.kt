package com.example.contando

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import org.jetbrains.anko.*
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

}


    fun ingresar(view: android.view.View) {
        val intent = Intent(this, home_user::class.java)
        if (textUser.getText().toString().isEmpty() || textPw.getText().toString().isEmpty()){
            alert("Hay campos vacíos"){

            }.show()
        }else
            startActivity(intent)
    }
    fun registrar(view: android.view.View) {
        val intent = Intent(this, registro::class.java)
        startActivity(intent)
    }

}